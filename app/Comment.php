<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Comment extends Model
{
    public function Products()
    {
        return $this->belongsTo(Product::class);
    }
    
    protected $fillable = [
        'body','product_id'
    ];
}
