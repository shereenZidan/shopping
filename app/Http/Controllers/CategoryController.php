<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;

class CategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
     public function create()
    {
      return view('CreateCategory');
    }
    
    public function store()
    {
        /*$category = new category;
        $category->name= request('name');
        $category->description= request('description');
        $category->save();*/
        
         $this->validate(request(),[
            'name' => 'required',
            'description' => 'required'
        ]);
        
        category::create
            ([
                'name'=> request('name'),
                'description'=> request('description')
            ]);
    }
    
    public function getData()
    {
        $categories=category::all();
//        $productid= Auth::product()->id;
//        $Category= Product::find($productid)->Categories;
        return view('Categories',['categories'=> $categories]);
        
    }
}


