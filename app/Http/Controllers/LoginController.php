<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
        
    {
        $this->middleware('guest');
    }
    
    public function create()
    {
      return view('login');
    }
    
    public function store()
    {
        if(!auth()->attempt(request(['username','password'])))
        {
            
            return back()->withErrors([
                'message'=> 'Please check your email and password and try again.'
            ]);
        }
        else
        {
            return redirect('HomePage');
        }
    }
}
