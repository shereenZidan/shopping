<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use Auth;
use Carbon\Carbon;

class ProductController extends Controller
{
     public function create()
    {
      return view('CreateProduct');
    }
    
    public function show(Product $Product)
    {
//        $Product = Product::find($id);
        
        return view('show',compact('Product'));
    }
    
    public function store(Request $request)
    {
        
       /* $this->validate(request(),[
            'name' => 'required',
            'description' => 'required'
            'price' => 'required'
            'quantity' => 'required'
        ]);*/
        
        /*$user= new User();
        $user->name=request('name');
        $user->description=request('description');
        $user->price=request('price');
        $user->quantity=request('quantity');
        
        $user= User::create(request(['username','password']));
        
        $user->save();*/
        
        $id = Auth::user()->id;
        $product = new Product;
        $product->name = $request->name;
        $product->user_id=$id;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();
    }
    
    //find btakhod Pk
    public function getData()
    {
        /*$Products=Product::all();*/
        $userid= Auth::user()->id;
        /*$Products = Product::where('user_id', $userid)->all();*/
        $Products = User::find($userid)->products();
        if($month=request('month'))
        {
            $Products->whereMonth('created_at', Carbon::parse($month)->month);

        }
        
        if($year=request('year'))
        {
            $Products->whereYear('created_at',$year);
        }
        
      $Products=$Products->get();
        
        $archives= Product::selectRaw('year(created_at)year, monthname(created_at)month, count(*)published')->groupBy('year','month')
        ->orderByRaw('min(created_at)asc')
        ->get()
        ->toArray();
            
        return view('Products', compact('Products','archives') );
    }
    
}
//['Products'=> $Products] 
//$Products=Product::all();
//return view('Products', compact('Products') );
