<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register');
    }
    
    public function store()
    {
        $this->validate(request(),[
            'username' => 'required|unique:users',
            'password' => 'required|confirmed'
        ]);
        
        $user= new User();
        $user->username=request('username');
        $user->password=Hash::make(request('password'));
        
        /*$user= User::create(request(['username','password']));*/
        
        $user->save();
        
        return redirect('HomePage');
    }
}



