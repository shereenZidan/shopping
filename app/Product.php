<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\User;

class Product extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
//    public function categories()
//    {
//        return $this->belongsTo(category::class);
//    }
    
    public function Comments()
    {
        return $this->hasMany(Comment::class);
    }
}
