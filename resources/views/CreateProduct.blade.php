@extends ('HomePage')

@section ('content')

 <h1>Add product</h1>

<form action="/CreateProduct" method="post">
    {{csrf_field()}}
    
    <div class="form-group">
        
       <input class="form-control" name="name" placeholder="name" type="text" autocomplete="on" required>
        
    </div>
    
     <div class="form-group">
        
       <input class="form-control" name="description" placeholder="description" type="text" autocomplete="on" required>
        
    </div>
    
    <div class="form-group">
        
      <input class="form-control" name="price" placeholder="price" type="number" required>
        
    </div>
    
    <div class="form-group">
        
      <input class="form-control" name="quantity" placeholder="quantity" type="number" required>
        
    </div>
    
    
    <button type="submit" class="btn btn-primary" >Submit</button>
    
</form>
<br>


@endsection