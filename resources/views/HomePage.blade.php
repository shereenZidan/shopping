<!doctype html>
<html>
    <head>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
         <link href="/css/style.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        
       <!-- <nav id="nav">
         <section >
         <ul id="menu">
             
            <li class="btn btn-info"><a href="logout"><i>Log Out</i></a></li>
            <li class="btn btn-info"><a href="reset.php"><i>Reset password</i></a></li>
                
         </ul>
        </section>
             
    </nav>
        
        <h1>Welcome</h1>
    -->
        
        
        
        
        <div id="app">
        
            @include ('layouts.nav')
            
            <div class="container">
                <div class="container" style="display: block; margin-left: auto; margin-right: auto; width: 40%;">
                     <img alt="Shop" src="/img/logo.png" />
                </div>

        @yield('content')
    </div>
        
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!--<div id="bottom">
        The best shop in the world © All rights received
    </div>--> 
        </div>
        
        
        @include ('layouts.footer')

    </body>
    
    
</html>

