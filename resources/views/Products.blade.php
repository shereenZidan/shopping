

<!--
<h1>Display the products</h1>

<table class="table-bordered table table-striped">
    <thead class="thead-dark">
    <tr>
    <th>Name</th>
    <th>Description</th>
    <th>Price</th>
    <th>Quantity</th>
    </tr>
    </thead>
    @foreach($Products as $Product) 
    <tr><td>{{$Product->name}}</td>
    <td>{{$Product->description}}</td>
    <td>{{$Product->price}}</td>
    <td>{{$Product->quantity}}</td></tr>
    
    @endforeach
</table>
<div>
    <p>What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
</div>
<div>
    <p>
        Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


    </p>
</div>

<div>
    <p>
        Where does it come from?
Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
    </p>
</div>

<table class="infotable">
    
    <tr>
        <td>Name</td>
        <td>Description</td>
        <td>Price</td>
        <td>Quantity</td>
    </tr>
    
</table>

-->


<!DOCTYPE html>
<html lang="en"> 
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    

    <title>Shopping</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet" type="text/css">

  </head>

  <body>
<div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
            
            
          <a class="blog-nav-item active"  href="#">Home</a>
          <a class="blog-nav-item" href="#">New features</a>
          <a class="blog-nav-item" href="#">Press</a>
          <a class="blog-nav-item" href="#">New hires</a>
          <a class="blog-nav-item" href="#">About</a>
        </nav>
      </div>
    </div>
    <div class="container">

    <div class="container">
      <div class="blog-header">
        <h1 class="blog-title">View Products</h1>
        <p class="lead blog-description">Here you can view your products.</p>
          <hr>
      </div>
        </div>

    <div class="container">

      <div class="row">

        <div class="col-sm-8 blog-main">
          <div class="blog-post">
              @foreach ($Products as $Product)
              <a href="/Products/{{$Product->id}}">
            <h2 class="blog-post-title">{{$Product->name}}</h2>
                  </a>
                
            <p class="blog-post-meta">
                
           Added by {{$Product->users['username']}} on
                
            {{$Product->created_at->toFormattedDateString()}}
            
            </p>
              
            {{$Product->description}}
              @endforeach

            </div><!-- /.blog-post -->
            

<!--
          <div class="blog-post">
            <h2 class="blog-post-title">Another blog post</h2>
            <p class="blog-post-meta">December 23, 2013 by <a href="#">Jacob</a></p>

            
          </div> /.blog-post 
-->

<!--
          <div class="blog-post">
            <h2 class="blog-post-title">New feature</h2>
            <p class="blog-post-meta">December 14, 2013 by <a href="#">Chris</a></p>

          </div> /.blog-post 
-->


        </div><!-- /.blog-main -->

        <div class="col-sm-3 offset-sm-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
                @foreach ($archives as $stats)
                <li>
                    <a href="?month={{ $stats['month'] }}&year={{ $stats['year'] }}">
                        {{$stats['month'].' '.$stats['year']}}
                    </a>
                </li>
                @endforeach
                
             
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

      </div>
      
      @include ('layouts.footer')
    
  </body>
</html>




