<!doctype html>
<html>
    <head>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        
    </head>
<body>
    
    <br>

    <div class="container">
    <div class="center" >
        <img   alt="Shop" src="/img/logo.png" />
    </div>
    </div>
    
   <br>
    
    <div class="container">
<form action="/login" method="post">
    {{csrf_field()}}    
    
    <div class="form-group">
        
        <h1><strong>Sign In</strong></h1>
        
       <input class="form-control" name="username" placeholder="username" type="text" autocomplete="on">
        
    </div>
    
    <div class="form-group">
        
      <input class="form-control" name="password" placeholder="password" type="password">
    </div>
    
    <div class="form-group">
    <button type="submit" class="btn btn-primary">Sign In</button>
    </div>
     @include ('layouts.errors')
</form>
    </div>
   
</body>
</html>