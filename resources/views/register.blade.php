<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
    </head>
<body>
    <br>
    <br>
    
    <div class="container">
<form action="/register" method="post">
    {{csrf_field()}}
    
    <div class="form-group">
        
       <input class="form-control" name="username" placeholder="username" type="text" autocomplete="on">
        
    </div>
    
    <div class="form-group">
        
      <input class="form-control" name="password" placeholder="password" type="password">
    </div>
    
    <div class="form-group">
        
      <input class="form-control" name="password_confirmation" placeholder="Re-type password" type="password">
    </div>
    
    <div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
        <div class="form-group">
            @include('layouts.errors')
        </div>
        </div>
</body>
</html>