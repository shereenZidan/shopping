
<!DOCTYPE html>
<html lang="en"> 
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    

    <title>Shopping</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet" type="text/css">

  </head>
    <body>
        
@include ('layouts.nav')

      <div class="container">
    
  <strong> <h1> {{$Product->name}} </h1> </strong>
    
    {{$Product->description}}
    
</div>
        <hr>
<div class="container"><h1>Comments..</h1>

    @foreach ($Product->comments as $comment)
    <li class="list-group-item">
        <strong>
            {{$comment->created_at->diffForHumans()}}:
        </strong>
        {{$comment->body}}
    </li>
    @endforeach
</div>
        <br>
<div class="container"> 
<div class="card">
    <div class="card-block">
        <form method="post" action="/Products/{{$Product->id}}/comments">
            {{csrf_field()}}
            <div class="form-group">
                <textarea name="body" placeholder="Your comment here.." class="form-control"></textarea>
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add comment</button>
            </div>
        </form>
        
    </div>
        </div>
    </div>
        
        

        
@include ('layouts.footer')
        

</body>

</html>