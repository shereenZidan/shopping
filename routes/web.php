<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'RegisterController@create')->name('register');//ama kont shyla name de ama kont baagy a3ml register kan bytl3 error msh bywdyny ll hp
Route::get('/login', 'LoginController@create');

Route::post('/register', 'RegisterController@store')->name('register');
Route::post('/login','LoginController@store');

Route::get('/HomePage', function () {
    return view('HomePage');
})->name('HomePage');


Route::get('/logout', 'LogoutController@destroy')->name('logout');
Route::post('/logout', 'LogoutController@destroy')->name('logout');

Route::get('/CreateProduct', 'ProductController@create');
Route::post('/CreateProduct', 'ProductController@store');

Route::get('/Products','ProductController@getData');
Route::post('/Products','ProductController@getData');
Route::get('/Products/{Product}','ProductController@show');

Route::get('/CreateCategory','CategoryController@create');
Route::post('/CreateCategory','CategoryController@store');

Route::get('/Categories','CategoryController@getData');
Route::post('/Categories','CategoryController@getData');

Route::post('/Products/{Product_id}/comments', 'commentsController@store');
